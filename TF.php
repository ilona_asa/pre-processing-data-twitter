<?php
include('konek.php');

function term_frequency($id, $data, $size){
    $sum = 0;
    $tf = array_fill(0, 369, 0);
    for($i=0; $i<$size; $i++){
        $index = 0;
        $retval = mysql_query("SELECT kata FROM features");
        while($s=mysql_fetch_array($retval)){
            if($data[$i] == $s['kata']){
                $tf[$index]++;
                $sum++;
                continue;
            }
            $index++;
        }
    }
    $max = get_max($tf);
    
    //loop laplace
    for($k=0; $k<369; $k++){
        $weight = laplace($sum, $max, $tf[$k]);
        $tf[$k] = $weight;
    }
    
    $file = 'data.csv';
    $string = $id;
    for($j=0; $j<369; $j++){
        $string .= ",".$tf[$j];
    }
    $string .= ",".$sum;
    $string .= ",".$max;
    $insert = file_put_contents($file, $string."\n", FILE_APPEND | LOCK_EX);
}

function word_count($id, $data, $size){
    $sum = 0;
    $tf = array_fill(0, 700, 0);
    for($i=0; $i<$size; $i++){
        $index = 0;
        $retval = mysql_query("SELECT kata FROM features_sinonim");
        while($s=mysql_fetch_array($retval)){
            if($data[$i] == $s['kata']){
                $tf[$index]++;
                $sum++;
                continue;
            }
            $index++;
        }
    }    
    
    
    $file = 'data_count.csv';
    $string = "$id";
    for($j=0; $j<700; $j++){
        $string .= ",".$tf[$j];
    }
    $string .= ",".$sum;
    $insert = file_put_contents($file, $string."\n", FILE_APPEND | LOCK_EX);

    
}

function get_max($data){
    $max = 0;
    for($i=0; $i<369; $i++){
        if($max<$data[$i]){
            $max = $data[$i];
        }
    }
    return $max;
}

function laplace($N, $X, $c){
    $k = 1;
    if($N > 0){
        $weight = ($c+$k)/($N+($k*$X));
        return $weight;
    }else{
        return 0;
    }
}

?>